/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.test.product.service;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.math.BigDecimal;

import com.test.product.entity.Product;
import com.test.product.entity.ProductDetail;

import com.test.product.repository.ProductRepository;
import com.test.product.repository.ProductDetailRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.when;


import org.springframework.boot.web.client.RestTemplateBuilder;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {
    
    @Mock
    private ProductRepository productRepository;
    
    @Mock
    private ProductDetailRepository productDetailRepository;
    
    @Mock
    private RestTemplateBuilder restTemplateBuilder;
    
    @InjectMocks
    private ProductService productService;
    
    private Product productOne;
    private ProductDetail detailOne;
    private ProductDetail detailTwo;
    private final List<ProductDetail> listOne = new ArrayList<>();
    
    private Product productTwo;
    private ProductDetail detailThree;
    private ProductDetail detailFour;
    private final List<ProductDetail> listTwo = new ArrayList<>();
    
    private Product productThree;
    private ProductDetail detailFive;
    private ProductDetail detailSix;
    private final List<ProductDetail> listThree = new ArrayList<>();
    
    @BeforeEach
    void init() throws Exception{
        
        productTwo =  new Product();
        productTwo.setId(1L);
        productTwo.setName("t-shirt");
        
        detailThree = new ProductDetail();
        detailThree.setId(2L);
        detailThree.setColor("red");
        detailThree.setPrice(BigDecimal.TEN);
        
        detailFour = new ProductDetail();
        detailFour.setId(3L);
        detailFour.setColor("blue");
        detailFour.setPrice(BigDecimal.ONE);
        
        listTwo.add(detailThree);
        listTwo.add(detailFour);
        
        productTwo.setDetails(listTwo);
        
        
        productThree =  new Product();
        productThree.setId(1L);
        productThree.setName("playera");
        
        detailFive = new ProductDetail();
        detailFive.setId(2L);
        detailFive.setColor("black");
        detailFive.setPrice(BigDecimal.ONE);
        
        detailSix = new ProductDetail();
        detailSix.setId(3L);
        detailSix.setColor("blue");
        detailSix.setPrice(BigDecimal.ONE);
        
        listThree.add(detailFive);
        listThree.add(detailSix);
        
        productThree.setDetails(listThree);
    
    }
    
    @Test
    void createProductTest(){
        
        productOne =  new Product();
        productOne.setName("t-shirt");
        
        detailOne = new ProductDetail();
        detailOne.setColor("red");
        detailOne.setPrice(BigDecimal.TEN);
        
        detailTwo = new ProductDetail();
        detailTwo.setColor("blue");
        detailTwo.setPrice(BigDecimal.ONE);
        
        listOne.add(detailOne);
        listOne.add(detailTwo);
        
        productOne.setDetails(listOne);
        
        when(productRepository.save(productOne)).thenReturn(productTwo);
        when(productDetailRepository.saveAll(listOne)).thenReturn(listTwo);
        
        Product result = productService.createProduct(productOne);
        
        assertEquals(1L, result.getId());
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
                
        assertEquals(2L, result.getDetails().get(0).getId());
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertEquals(3L, result.getDetails().get(1).getId());
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
    
    }
    
    @Test
    void updateProductTest(){
        
        when(productRepository.save(productTwo)).thenReturn(productThree);
        when(productDetailRepository.saveAll(listTwo)).thenReturn(listThree);
        
        productTwo.setName("playera");
        productTwo.getDetails().get(0).setColor("black");
        productTwo.getDetails().get(0).setPrice(BigDecimal.ONE);
        
        Product result = productService.updateProduct(productTwo);
        
        assertTrue(result.getId() > 0L);
        assertEquals("playera", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("black", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
    
    }
    
    @Test
    void getProductByIdTest(){
        
        productOne =  new Product();
        productOne.setName("t-shirt");
        
        detailOne = new ProductDetail();
        detailOne.setColor("red");
        detailOne.setPrice(BigDecimal.TEN);
        
        detailTwo = new ProductDetail();
        detailTwo.setColor("blue");
        detailTwo.setPrice(BigDecimal.ONE);
        
        listOne.add(detailOne);
        listOne.add(detailTwo);
        
        productOne.setDetails(listOne);
        
        when(productRepository.save(productOne)).thenReturn(productTwo);
        when(productDetailRepository.saveAll(listOne)).thenReturn(listTwo);
        
        Product result = productService.createProduct(productOne);
        
        assertEquals(1L, result.getId());
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
                
        assertEquals(2L, result.getDetails().get(0).getId());
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertEquals(3L, result.getDetails().get(1).getId());
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
        
        when(productRepository.findById(1L)).thenReturn(Optional.of(productTwo));
        
        result = productService.getProductById(1L);
        
        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
    
    }
    
}
