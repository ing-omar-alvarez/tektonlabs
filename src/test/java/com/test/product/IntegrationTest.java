package com.test.product;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import com.test.product.entity.Product;
import com.test.product.entity.ProductDetail;
import com.test.product.service.ProductService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author ignis
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {
    
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Autowired
    private ProductService productService;
    
    private ObjectMapper mapper;
    
    private HttpEntity<Object> entity;
    private HttpHeaders headers = new HttpHeaders();
    
    private String path;
    private final String context = "/dev/api/products";
    
    private Product productOne;
    private ProductDetail detailOne;
    private ProductDetail detailTwo;
    private final List<ProductDetail> list = new ArrayList<>();
    
    
    @BeforeEach
    void init() throws Exception{
        
        productOne =  new Product();
        productOne.setName("t-shirt");
        
        detailOne = new ProductDetail();
        detailOne.setColor("red");
        detailOne.setPrice(BigDecimal.TEN);
        
        detailTwo = new ProductDetail();
        detailTwo.setColor("blue");
        detailTwo.setPrice(BigDecimal.ONE);
        
        list.add(detailOne);
        list.add(detailTwo);
        
        productOne.setDetails(list);
        
        path = "http://localhost:" + port + context;
        mapper = new ObjectMapper();
    
    }
    
    @Test
    void saveProductAndDetailsTest(){
        
        Product result = productService.createProduct(productOne);
        
        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
    }
    
    @Test
    void updateProductAndDetailsTest(){
        
        Product result = productService.createProduct(productOne);
        
        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
        
        result.setName("playera");
        result.getDetails().get(0).setColor("black");
        result.getDetails().get(0).setPrice(BigDecimal.ONE);
        result = productService.updateProduct(result);
        
        assertTrue(result.getId() > 0L);
        assertEquals("playera", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("black", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
        
    }
    
    @Test
    void findProductByIdTest(){
        
        Product result = productService.createProduct(productOne);
        
        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
        
        Long id = result.getId();
        
        result = productService.getProductById(id);
        
        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN.setScale(2), result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE.setScale(2), result.getDetails().get(1).getPrice());
    }
    
    @Test
    void postProductTest() throws Exception{
        
        entity = new HttpEntity<>(productOne, headers);
        ResponseEntity<String> response = this.restTemplate
                .exchange(path,HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        Product result = mapper.readValue(response.getBody(), Product.class);

        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
    
    }
    
    @Test
    void putProductTest() throws Exception{
        
        entity = new HttpEntity<>(productOne, headers);
        ResponseEntity<String> response = this.restTemplate
                .exchange(path,HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        Product result = mapper.readValue(response.getBody(), Product.class);

        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
        
        
        result.setName("playera");
        result.getDetails().get(0).setColor("black");
        result.getDetails().get(0).setPrice(BigDecimal.ONE);

        entity = new HttpEntity<>(result, headers);
        response = this.restTemplate
                .exchange(path,HttpMethod.PUT, entity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        result = mapper.readValue(response.getBody(), Product.class);

        assertTrue(result.getId() > 0L);
        assertEquals("playera", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("black", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
    
    }
    
    @Test
    void getProductByIdTest() throws Exception{
        
        entity = new HttpEntity<>(productOne, headers);
        ResponseEntity<String> response = this.restTemplate
                .exchange(path,HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        Product result = mapper.readValue(response.getBody(), Product.class);

        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN, result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE, result.getDetails().get(1).getPrice());
        
        Long id = result.getId();
        
        entity = new HttpEntity<>(headers);
        response = this.restTemplate
                .exchange(path + "/" + id,
                        HttpMethod.GET, entity, String.class);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());

        result = mapper.readValue(response.getBody(), Product.class);
        
        assertTrue(result.getId() > 0L);
        assertEquals("t-shirt", result.getName());
        assertEquals(2, result.getDetails().size());
        
        assertTrue(result.getDetails().get(0).getId() > 0L);
        assertEquals("red", result.getDetails().get(0).getColor());
        assertEquals(BigDecimal.TEN.setScale(2), result.getDetails().get(0).getPrice());
        
        assertTrue(result.getDetails().get(1).getId() > 0L);
        assertEquals("blue", result.getDetails().get(1).getColor());
        assertEquals(BigDecimal.ONE.setScale(2), result.getDetails().get(1).getPrice());
    
    }
}
