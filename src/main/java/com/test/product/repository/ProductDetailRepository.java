/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.test.product.repository;

import com.test.product.entity.ProductDetail;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ignis
 */
@Repository
public interface ProductDetailRepository extends JpaRepository<ProductDetail, Long> {
    
}
