
package com.test.product.repository;

import java.util.Optional;
import com.test.product.entity.Product;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author ignis
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    
    //Optional<Product> findById(Long id);
}
