
package com.test.product.controller;

import com.test.product.entity.Product;
import com.test.product.service.ProductService;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ignis
 */
@CrossOrigin
@RestController
@RequestMapping("/products")
public class ProductController {
    
    private final ProductService productService;
    
    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    
    @PostMapping
    public ResponseEntity<Object> createProduct(@Valid @RequestBody Product product) throws Exception {

        Product result = productService.createProduct(product);
        return ResponseEntity.ok(result);
    }
    
    @PutMapping
    public ResponseEntity<Object> updateProduct(@Valid @RequestBody Product product) throws Exception {

        Product result = productService.updateProduct(product);
        return ResponseEntity.ok(result);
    }
    
    @GetMapping("/{productId}")
    public Product getProductById(@PathVariable Long productId) {
        return productService.getProductById(productId);
    }
    
}
