package com.test.product.service;

import java.util.List;
import java.util.Optional;
import java.util.Collections;
import javax.transaction.Transactional;

import com.test.product.entity.Product;
import com.test.product.entity.ProductDetail;
import com.test.product.repository.ProductRepository;
import com.test.product.repository.ProductDetailRepository;

import org.springframework.http.MediaType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
/**
 *
 * @author ignis
 */
@Service
@Transactional
public class ProductService {
    
    private final ProductRepository productRepository;
    private final ProductDetailRepository productDetailRepository;
    
    private HttpEntity<Object> entity;
    private HttpHeaders headers;
    private final String path = "https://somurl/";
    private final RestTemplate restTemplate;

    @Autowired
    public ProductService(ProductRepository productRepository, 
            ProductDetailRepository productDetailRepository,
            RestTemplateBuilder restTemplateBuilder) {
        this.productRepository = productRepository;
        this.productDetailRepository = productDetailRepository;
        this.restTemplate = restTemplateBuilder.build();
        
        headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    }
    
    public Product createProduct(Product product){
        
        Product result = productRepository.save(product);
        List<ProductDetail> details = updateDetail(product, product.getDetails());
        if(details.size() > 0){
            productDetailRepository.saveAll(details);
        }
        return result;
    }
    
    public Product updateProduct(Product product){
        Product result  = null;
        if(product.getId() != null){            
            result = productRepository.save(product);
            productDetailRepository.saveAll(product.getDetails());
        }
        else{
            //Here should throws an error
        }
        
        return result;
    }
    
    public Product getProductById(Long id){
        
        Optional<Product> result =  productRepository.findById(id);
        
        return result.orElse(null);
    }
    
    private List<ProductDetail> updateDetail(Product product, List<ProductDetail> details){
        
        for(ProductDetail detail : details){
            if(detail.getProduct() == null)
                detail.setProduct(product);
        }
        
        return details;
    }
    
    //with this method you can get the body of the url of external resource
    private String getNameFromExternalResource(Long id){
        entity = new HttpEntity<>(headers);
        ResponseEntity<String> response  = this.restTemplate
                .exchange(path + id,HttpMethod.GET, entity, String.class);
        
        if(response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            return null;
        }
    }
    
}
